# atropos1789.gitlab.io

Built with Hugo v0.124.1+extended

## Theme

The theme used for this website is a personal fork of <https://themes.gohugo.io/themes/smol/>.


## GitLab

This repository is partially derived from the sample Hugo repository provided by GitLab, located at <https://gitlab.com/pages/hugo>.


## Math Support

The code to support using KaTeX to render math is pulled from <https://gohugo.io/content-management/mathematics/>.
An alternative method that may be implemented in the future can be found [here](https://mertbakir.gitlab.io/hugo/math-typesetting-in-hugo/).


## TODO

Make the main menu more diverse than just a series of entries to categories of posts
- a research page that's more formatted and talks about current projects
- home pages for diff types of posts that don't have the weird "TechES" naming scheme
