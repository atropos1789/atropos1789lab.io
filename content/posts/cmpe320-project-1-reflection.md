+++
title = 'CMPE320 Project 1 Reflection'
date = 2024-02-26T23:44:48-04:00
slug = '2024-02-26-cmpe320-project-1-reflection'
type = 'posts'
draft = false
+++

**written by kira singla**

## The Project

The code for the project in question is located in a git repo [here](https://gitlab.com/atropos1789/cmpe320-project-1). This code has been updated since the project was submitted, and it was not originally in a git repository. The file `cmpe320-project-1-description.pdf` contains the project specifications and problems.


## On Not Using Python

This project has been a massive learning experience for me, although a lot of it has been because I made the project substantially harder than it needed to be by choosing to use a compiled programming language (Fortran 2008) instead of using an interpreted one such as MATLAB or Python. 
This decision was made in equal parts because of my curiosity about compiled languages and my general desire to operate on as low of a level as possible when programming. 
In the past I have used Python and MATLAB for research projects, but found that the rapid development process made it very easy to lose a stable development branch and have to spend time backtracking. At the time of writing this, I am not fluent in git to the extent that I could use it for a school project, so I am instead using manual version control via saving to new filepaths at certain intervals. I intend to use git for future projects, which will also make it easier to keep with the rapid development characteristic of interpreted languages. This skill will also help as I work in collaborative environments including research and work.


## On Using Fortran

In this project, I opted to use a compiled language where it would be easier to separate the source files and the binaries, and to make new copies of the source for each time I compiled. 
I learned a lot about research documentation while working in this way, including how to maintain several active development branches for when I have multiple ideas I want to play around with, and how to manage a more involved build process through the use of Makefiles. 
I also had to learn about (and compile!) a whole new library to use for graphing with Fortran, called [PLPlot](https://plplot.sourceforge.net/).
This itself was a large challenge and it took me time to familiarize myself with the tool, but it taught me a lot about using software libraries because the library is not bound to any specific language, so I had to interpret the documentation for use with Fortran. 

Fortran does not have intrinsics to generate numbers from distributions, it only has a function to generate a random float between 0 and 1. 
In the process of finding tools to obtain a sample from a distribution, I learned about the mathematics of transforming distributions.
In effect, I had to find a way to transform a uniform distribution into other types of distributions. 
I was able to find source code that demonstrated the appropriate transformations for the exponential and normal distributions, and learned that doing this for normal distributions is actually not that straightforward, and can be done through a variety of algorithms of varying accuracy, including the Box Muller Algorithm (which was the default of the  library I ended up using). 
Being able to go through the code and documentation for these libraries taught me that a lot of these algorithms are very new and the code itself references academic papers published within my lifetime. 
This is very motivating for me personally, because I am interested in the application of academic discoveries to computer algorithms, and I find myself motivatved to follow the example of a lecturer hosted recently, who has published papers on an algorithm and coded several implementations of it that are freely available. 
Now that I've learned how to handle versions of my code more strictly, I feel more comfortable using interpreted languages for projects, however I am still not a fan of Python, so I will be chosing between Fortran and Julia (and potentially C) for future projects.  
