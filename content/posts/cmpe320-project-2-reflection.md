+++
title = 'CMPE320 Project 2 Reflection'
date = 2024-03-25T16:36:03-04:00
slug = '2024-03-25-cmpe320-project-2-reflection'
type = 'posts'
draft = false
+++

**written by kira singla**

## The Project

The source code for this project can be found in a git repository at <https://gitlab.com/atropos1789/cmpe320-project-2/>. This code has not been updated since it was written, however it was not written with git, so the repo has a flat history. The file `cmpe320-project-2-description.pdf` contains the project's specifications and problems.

## Using Julia

This project, much more than the previous one, has been a journey of refinement and improvement. 
Whereas I spent the previous project experimenting with new tools (Fortran and PLplot), I decided to use tools more familiar to me this time.
Julia as a language isn't something I have much experience with, however it works a lot like python, and the graphing tool I used is actually written in python (I also have experience using this tool in python). 
I have previously worked with batch data (as in project 1, but also other places), however many of the strategies I have used have been unrefined and poorly organized, making it hard to scale them. 
For this project, I took advantage of the fact that Julia doesn't require function interfaces or modules in order to build a low effort but highly modular codebase for my project, which cut my code re-use by a large portion. 
Not only was this more efficient, it eliminated sources of error where I'd have to keep several identical blocks of code updated with improvements.
Organizing functions across different files had the added benefit of shortening file sizes and making it easier to find the code block I want to edit.


## Using HDF5

For this project, I also opted to save the random data created in the first problem for re-use in the later problems instead of regenerating it. 
One option to do this would have been saving the variable that stores the random data as a global variable, so then it wouldn't be deallocated during the runtime, however I was not developing the code by running `main.jl` each time and I did not want to, so this would have been tedious.
I instead identified a method to save the data to disk, using the HDF5 library. 
There was substantial complexity in fully saving the data with HDF5, so I decided to use JDL2.jl instead, which provided a simplified interface and a custom re-programmed library optimized for use with Julia. 
The trade-off of JLD2.jl is that I lose compatibility with other languages and am locked into using Julia, however none of those were concerns on this small project.
In future research it will absolutely be advantageous to switch to HDF5, but I do not anticipate learning how to fully use HDF5.jl just for projects in this class.
