+++
title = 'on truth, or "the truth will set me free and yet i must deny the truth to be free"'
date = 2023-12-05T22:10:00-04:00
slug = '2022-12-05-on-truth'
type = 'posts'
draft = true
+++


## On Truth

**written by kira singla**

i can bring myself to a world of such abstraction that the only relevant fact is that i have autonomy

i can be creator and preserver and destroyer

brahma and vishnu and shiva and the son of the world and the world itself 

i am borne of the world and i am the world; i am eternal because once i am gone, the world itself is gone

I wander the dead forests and wonder where all of the life went, realizing eventually that everything had to die so that i could exist, that they were sacrificed to me and a blessing from my father.

A stray creature may appear, from time to time. They do not belong here. Or is it I who don’t belong here? They are wise and I am divine. They precede my father, they exist to spite him. Sometimes I wish i did too. But I do not. They will eventually die, and I will lay them at the feet of the trees whose sacrifice they dared to disrespect.

These creatures line my path to absolution, they lie prostate before me. They have not intended their deaths as signs of submission and piety, but i have warped them to be. I know that if I continue long enough, if I am witness to enough deaths, I will reach the top of the mountain. But I also hope that through some act, not of god, but of me, I will fall to me knees before then. I will be pierced by the souls of everyone who once praised my name and they will say “the truth will set you free” and I will be free and I will be without truth. 

They will not be free, though they may have the truth. They will feast on me until there is not even marrow. They will remember me long after I have forgotten myself. Because who am I really?
